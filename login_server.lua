﻿symbols = {
"A","b","D","G","N","f","j","T","R","e","z","Z","X","x","V","v",'E',"H",'h', 's', 'S', 'a', 'B','n','m','M','l','L','g','F','y','Y','U','u','p','P'

}

function createRandomPassword (sym_num)
	local pass = ""
	for i = 1, sym_num do
		if math.random (1,2) == 1 then
			pass = pass..tostring(math.random(0,9))
		else
			pass = pass..symbols[math.random(1,#symbols)]
		end
	end
	return pass
end

function loginPlayerBySeralServer()
	local pl_acc = Account.getAllBySerial(source.serial)
	local login_name = createRandomPassword (10)
	local pass = createRandomPassword (10)
	--local pass = "1234" 
	if #pl_acc == 0 then
		outputChatBox ( 'Ваш логин: '..login_name, source)
		theAccount = addAccount(login_name, pass )
		if theAccount then
			logIn(source,theAccount, pass)
			triggerEvent("onPlayerDayZRegister", getRootElement(), getAccountName (theAccount), pass, source)
			--triggerClientEvent(source, "onPlayerDoneLogin", source, login_name, pass)
		else
			reason = "Unknown Error!"
			outputChatBox("[Login]#FF9900 " .. reason, source, 255, 255, 255, true)
		end
	else
		theAccount = pl_acc[1]
		if theAccount then
			outputChatBox ( 'Seu login: '..getAccountName (theAccount), source)
			setAccountPassword ( theAccount, pass )
			logIn(source,theAccount, pass)
			--logIn(source,theAccount, "1234")
			--triggerClientEvent(source, "onPlayerDoneLogin", source, getAccountName (theAccount), pass)
			triggerEvent("onPlayerDayZLogin", getRootElement(), getAccountName (theAccount), pass, source)
		else
			reason = "Unknown Error!"
			outputChatBox("[Login]#FF9900 " .. reason, source, 255, 255, 255, true)
		end
	end
end

addEvent("loginPlayerBySeral", true)
addEventHandler("loginPlayerBySeral", getRootElement(), loginPlayerBySeralServer)

function playerWantToQuitKick ()
	kickPlayer ( source, "Saída" )
end

addEvent("playerWantToQuit", true)
addEventHandler("playerWantToQuit", getRootElement(), playerWantToQuitKick)


addEventHandler("onPlayerJoin", getRootElement(), function()
	local pl_acc = Account.getAllBySerial(source.serial)
	if #pl_acc ~= 0 then
		local cloth0 = getAccountData ( pl_acc[1], "cloth0" ) or 0
		local cloth2 = getAccountData ( pl_acc[1], "cloth2" ) or 0
		local cloth3 = getAccountData ( pl_acc[1], "cloth3" ) or 0
		local cloth15 = getAccountData ( pl_acc[1], "cloth15" ) or 0
		local cloth16 = getAccountData ( pl_acc[1], "cloth16" ) or 0
		setElementData ( source, "cloth0", cloth0 )
		setElementData ( source, "cloth2", cloth2 )
		setElementData ( source, "cloth3", cloth3 )
		setElementData ( source, "cloth15", cloth15 )
		setElementData ( source, "cloth16", cloth16 )
		triggerClientEvent ( source, "updateTestPedClothes", source, cloth0, cloth2, cloth3, cloth15, cloth16 )
	end
end
)
